[GtkTemplate (ui = "/org/gnome/Mockup/window.ui")]
public class Mockup.Window : Gtk.ApplicationWindow {
	[GtkChild]
	private PhoneScreen phone_screen;

	private SwipeTracker tracker;
	private SwipeTracker.Direction? direction;
	private int state;

	public Window (Gtk.Application app) {
		Object (application: app);

		Gtk.Settings.get_default ().gtk_application_prefer_dark_theme = true;

		phone_screen.add_events (Gdk.EventMask.ALL_EVENTS_MASK);
		state = 0;

		tracker = new SwipeTracker (phone_screen, Gtk.Orientation.VERTICAL);
		tracker.can_swipe_forward = true;

		tracker.begin.connect (dir => {
			direction = dir;
		});
		tracker.update.connect (progress => {
			phone_screen.progress = state - progress;
		});
		tracker.end.connect (cancel => {
			if (!cancel)
				state += (direction == SwipeTracker.Direction.BACK ? -1 : 1);
			tracker.can_swipe_back = state > 0;
			tracker.can_swipe_forward = state < 2;
		});
	}
}
